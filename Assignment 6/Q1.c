#include <stdio.h>

void Pattern(int a);
void Row(int b);
int x;
int NoOfRows = 1;

void Pattern(int a){

	if(a>0){
	
		Row(NoOfRows);
		printf("\n");
		NoOfRows++;
		Pattern(a-1);
	}
}

void Row(int b){

	if(b>0){
	
		printf("%d",b);
		Row(b-1);
	}
}
int main(){

	printf("Enter the Number: ");
	scanf("%d",&x);
	Pattern(x);

return 0;
}
