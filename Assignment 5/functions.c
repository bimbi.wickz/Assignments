#include <stdio.h>

int PerformanceCost = 500;
int CostPerAttendee = 3;

int Attendence(double price){
	return 120 - ((price - 15)/5)*20;	
}
double Cost(double price){
	return PerformanceCost + Attendence(price)*CostPerAttendee;
}
double Income(double price){
	return Attendence(price)*price;
}
double Profit(double price){
	return Income(price)-Cost(price);
}

int main(){

	double price;
	printf("Profits for ticket prices: \n");
	for(price=1; price<45; price++){
	
		printf("Ticket price: Rs.%.2f \t Profit: Rs.%.2f \n",price,Profit(price));

	}
return 0;
}

