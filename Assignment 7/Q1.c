#include <stdio.h>
#include <string.h>

int main(){

	char sentence[255];
	printf("Enter a sentence: ");
	scanf("%[^\n]%*c", sentence);

	for(int i=strlen(sentence); i>=0; i--){
	
		printf("%c",sentence[i]);
	}

	printf("\n");
}
